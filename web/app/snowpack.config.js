/** @type {import("snowpack").SnowpackUserConfig } */

const httpProxy = require("http-proxy");
const proxy = httpProxy.createServer({ target: "http://localhost:3000/" });

module.exports = {
  mount: {
    public: { url: "/", static: true },
    src: { url: "/dist" },
  },
  plugins: [
    "@snowpack/plugin-svelte",
    "@snowpack/plugin-dotenv",
    [
      "snowpack-plugin-wasm-pack",
      {
        projectPath: "../../parser",
      },
    ],
    "@snowpack/plugin-optimize",
  ],
  routes: [
    /* Example: Enable an SPA Fallback in development: */
    // {"match": "routes", "src": ".*", "dest": "/index.html"},
    {
      match: "all",
      src: "/api/.*",
      dest: (req, res) => {
        console.log(req.url);
        proxy.web(req, res);
      },
    },
    { match: "routes", src: ".*", dest: "/index.html" },
  ],
  optimize: {
    /* Example: Bundle your final build: */
    // "bundle": true,
  },
  packageOptions: {
    /* ... */
  },
  devOptions: {
    output: "stream", // dont clear terminal
  },
  buildOptions: {
    /* ... */
  },
};
