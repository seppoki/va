import {
  parseISO,
  format,
  formatDistanceToNowStrict,
  differenceInDays,
} from "date-fns";

export const formatTimestamp = (ts) => {
  let time = parseISO(ts);

  if (differenceInDays(new Date(), time) < 4) {
    return formatDistanceToNowStrict(time, { addSuffix: true });
  } else {
    return format(time, "d/M/Y");
  }
};

const wc = /\*/gm;
export const replaceWildcards = (str) => {
  return str.replace(wc, ":*");
};

export const groupBy = (array, key) => {
  const groups = array.reduce((result, current) => {
    (result[current[key]] = result[current[key]] || []).push(current);
    return result;
  }, {});
  return groups;
}
