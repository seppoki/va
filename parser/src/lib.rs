extern crate pest;
#[macro_use]
extern crate pest_derive;

use pest::Parser;
#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct Grammar;

use wasm_bindgen::prelude::*;
#[wasm_bindgen]
pub fn valid(input: &str) -> bool {
    if Grammar::parse(Rule::main, input).is_ok() {
        return true;
    }
    false
}
