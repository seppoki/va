# Vacancy-aggregator

Vacancy-aggregator collects job listings from multiple sources and provides a search engine to browse them.

The [va-crawler](https://gitlab.com/seppoki/va-crawler) occupies the backend with new entries.