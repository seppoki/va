import pgPromise from "pg-promise";
import { IInitOptions, IDatabase, IMain } from "pg-promise";
import { IExtensions, UsersRepository, VacanciesRepository, InfoRepository } from "./repos";
import { logger } from './logger';

const host = process.env.DB_HOST;
const port = parseInt(<string>process.env.DB_PORT, 10);
const database = process.env.DB_DATABASE;
const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;

logger.info('starting database connection with: host: %s, port %s, database %s, user %s', host, port, database, user);

if (!host || !port || !database || !user || !password) {
  logger.error('exiting: missing env variable(s)')
  process.exit(1);
}

type ExtendedProtocol = IDatabase<IExtensions> & IExtensions;

const initOptions: IInitOptions<IExtensions> = {
  extend(obj: ExtendedProtocol, dc: any) {
    obj.users = new UsersRepository(obj, pgp);
    obj.vacancies = new VacanciesRepository(obj, pgp);
    obj.info = new InfoRepository(obj, pgp);
  },
};

const pgp: IMain = pgPromise(initOptions);

// Creating the database instance with extensions:
const db: ExtendedProtocol = pgp({
  host,
  port,
  database,
  user,
  password,
});

export { User, Vacancy } from "./models";
export { db, pgp };
