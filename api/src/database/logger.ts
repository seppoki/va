import * as tracer from 'tracer';

export const logger = tracer.colorConsole({ level: process.env.DB_LOG_LEVEL! })
