/*
  -returns all fts matches from newest to oldest
  -optional pagination uses vacancy id as offset
  -optional limit
*/
SELECT v.id,
	v.url,
	v.header,
	v.source,
	v.ts,
	false AS hidden,
	false AS pinned,
  ts_rank(v.ts_simple, to_tsquery('simple', $<terms>)) +
  ts_rank(v.ts_finnish, to_tsquery('finnish', $<terms>)) +
  ts_rank(v.ts_english, to_tsquery('english', $<terms>)) AS rank
FROM (
	SELECT *
	FROM vacancies
	WHERE $<offsetClause:raw>
  ( ts_simple @@ to_tsquery('simple', $<terms>) OR
    ts_finnish @@ to_tsquery('finnish', $<terms>) OR
    ts_english @@ to_tsquery('english', $<terms>) )
  ) v
ORDER BY id DESC $<limitClause:raw>
