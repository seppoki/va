import { UsersRepository } from "./users";
import { VacanciesRepository } from "./vacancies";
import { InfoRepository } from "./info";

// Database Interface Extensions:
interface IExtensions {
  users: UsersRepository;
  vacancies: VacanciesRepository;
  info: InfoRepository;
}

export { IExtensions, UsersRepository, VacanciesRepository, InfoRepository };
