import { IDatabase, IMain } from "pg-promise";
import { User } from "../models";
import { users } from "../sql";
import { logger } from "../logger";

export class UsersRepository {
  constructor(private db: IDatabase<any>, private pgp: IMain) { }

  async add(email: string, pwhash: string): Promise<User | null> {
    logger.trace('querying database: adding an user: %s', email);
    return this.db.oneOrNone(users.add, { email, pwhash });
  }
  async getByEmail(email: string): Promise<User | null> {
    logger.trace('querying database: getting user by email: %s', email);
    return this.db.oneOrNone(users.getByEmail, { email });
  }
  async hideVacancy(userId: number, vacancyId: number): Promise<null> {
    logger.trace('querying database: user %d hides vacancy %d', userId, vacancyId);
    return this.db.none(users.hideVacancy, { userId, vacancyId });
  }
  async pinVacancy(userId: number, vacancyId: number): Promise<null> {
    logger.trace('querying database: user %d pins vacancy %d', userId, vacancyId);
    return this.db.none(users.pinVacancy, { userId, vacancyId });
  }
  async unhideVacancy(userId: number, vacancyId: number): Promise<null> {
    logger.trace('querying database: user %d unhides vacancy %d', userId, vacancyId);
    return this.db.none(users.unhideVacancy, { userId, vacancyId });
  }
  async unpinVacancy(userId: number, vacancyId: number): Promise<null> {
    logger.trace('querying database: user %d unpins vacancy %d', userId, vacancyId);
    return this.db.none(users.unpinVacancy, { userId, vacancyId });
  }
}
