import { IDatabase, IMain } from "pg-promise";
import { info } from "../sql";
import { logger } from "../logger";

export class InfoRepository {
  constructor(private db: IDatabase<any>, private pgp: IMain) { }

  async getCount(): Promise<Number | null> {
    logger.trace('querying database: getting vacancy count');
    return this.db.one(info.getCount);
  }
}
