import * as tracer from 'tracer';

export const logger = tracer.colorConsole({ level: process.env.API_LOG_LEVEL! })
