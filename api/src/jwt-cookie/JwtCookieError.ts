export class JwtCookieError extends Error {
  code: number;
  message: string;
  constructor(code: number, message: string) {
    super();
    Object.setPrototypeOf(this, JwtCookieError.prototype);
    this.code = code;
    this.message = message;
  }
}
