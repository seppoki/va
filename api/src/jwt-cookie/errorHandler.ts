import { ErrorRequestHandler } from "express";
import { JwtCookieError } from "./JwtCookieError";
import { logger } from "../logger";

export const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  try {
    if (err instanceof JwtCookieError) {
      logger.trace('jwt cookie error handler sending status code %d', err.code);
      return res.sendStatus(err.code);
    };
  } catch (err) {
    // handle unexpected errors gracefully
    return next(err);
  }
};
