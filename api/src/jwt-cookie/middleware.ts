import { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
import { JwtCookieError } from "./JwtCookieError";
import { logger } from "../logger";

declare global {
  namespace Express {
    interface Request {
      user: { id: number };
    }
  }
}

export const middleware = (
  key: jwt.Secret,
  ...config: Array<jwt.VerifyOptions>
) => {
  if (config.length === 0)
    throw new Error(
      "jsonwebtoken library verification function parameters not passed to middleware"
    );

  return async (req: Request, resp: Response, next: NextFunction) => {
    try {
      logger.trace("jtw cookie middleware: entry");
      // check that the required token is present
      if (!req.cookies || !req.cookies.token) {
        logger.trace("jtw cookie middleware: missing cookie token");
        return next(
          new JwtCookieError(401, "Missing authentication token cookie.")
        );
      }

      // verify the token
      try {
        const payload = jwt.verify(req.cookies.token, key, ...config);
        logger.trace("jtw cookie middleware: succesfully verified token");
        req.user = payload as { id: number };
        return next();
      } catch (err) {
        logger.trace("jtw cookie middleware: failed to verify token");
        return next(new JwtCookieError(401, "Authentication token is invalid"));
      }
    } catch (err) {
      logger.error("jtw cookie middleware: unexpected error: %s", err.message);
      // handle unexpected errors gracefully
      return next(err);
    }
  };
};
