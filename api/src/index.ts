import path from "path";
import dotenv from "dotenv";
dotenv.config({ path: path.join(__dirname, ".env") });
import express from "express";
import cors from 'cors';
import cookieParser from "cookie-parser";
import { middleware, errorHandler } from "./jwt-cookie";
import { me } from "./routes/me";
import { info } from "./routes/info";
import { guest } from "./routes/guest";
import { vacancies } from "./routes/vacancies";
import { authentication } from "./routes/authentication";
import { logger } from "./logger";

const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());

app.use("/api", authentication);
app.use("/api", info);
app.use("/api", guest);

app.use(
  middleware(process.env.JWT_SECRET_PUBLIC!.replace(/\\n/gm, '\n'), {
    algorithms: ["RS256"],
  })
);

app.use("/api", me);
app.use("/api", vacancies);

app.use(errorHandler);

const port = process.env.SERVER_PORT;
app.listen(port, () => {
  logger.info("api server listening on port %s", port)
});
