import express from "express";
import { logger } from "../logger";

let router = express.Router();
export const me = router.get("/me", async (req, res, next) => {
  try {
    logger.trace('api route: /me with user %d', req.user);
    if (!req.user) return res.status(401);
    return res.status(200).json(req.user);
  } catch (err) {
    logger.error('api route: /me unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});
