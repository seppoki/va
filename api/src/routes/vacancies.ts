import express from "express";
import { db as database, User } from "../database";
import { logger } from "../logger";

declare global {
  namespace Express {
    interface Request {
      user: { id: number };
    }
  }
}

let router = express.Router();
export const vacancies = router.post(
  "/vacancies/:id/pin",
  async (req, res, next) => {
    try {
      const userId = req.user.id;
      const vacancyId = Number(req.params.id);
      logger.trace('api route: /vacancies/%d/pin with vacancyId: %d', userId, vacancyId);
      if (!userId || !vacancyId) return res.sendStatus(400);

      await database.users.pinVacancy(userId, vacancyId);
      return res.sendStatus(200);
    } catch (err) {
      logger.error('api route: /vacancies/%d/pin unexpected error: %s', req.user.id, err.message);
      // handle unexpected errors gracefully
      return next(err);
    }
  }
);

router.post("/vacancies/:id/unpin", async (req, res, next) => {
  try {
    const userId = req.user.id;
    const vacancyId = Number(req.params.id);
    logger.trace('api route: /vacancies/%d/unpin with vacancyId: %d', userId, vacancyId);
    if (!userId || !vacancyId) return res.sendStatus(400);

    await database.users.unpinVacancy(userId, vacancyId);
    return res.sendStatus(200);
  } catch (err) {
    logger.error('api route: /vacancies/%d/unpin unexpected error: %s', req.user.id, err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.post("/vacancies/:id/hide", async (req, res, next) => {
  try {
    const userId = req.user.id;
    const vacancyId = Number(req.params.id);
    logger.trace('api route: /vacancies/%d/hide with vacancyId: %d', userId, vacancyId);
    if (!userId || !vacancyId) return res.sendStatus(400);

    await database.users.hideVacancy(userId, vacancyId);
    return res.sendStatus(200);
  } catch (err) {
    logger.error('api route: /vacancies/%d/hide unexpected error: %s', req.user.id, err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.post("/vacancies/:id/unhide", async (req, res, next) => {
  try {
    const userId = req.user.id;
    const vacancyId = Number(req.params.id);
    logger.trace('api route: /vacancies/%d/unhide with vacancyId: %d', userId, vacancyId);
    if (!userId || !vacancyId) return res.sendStatus(400);

    await database.users.unhideVacancy(userId, vacancyId);
    return res.sendStatus(200);
  } catch (err) {
    logger.error('api route: /vacancies/%d/unhide unexpected error: %s', req.user.id, err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.get("/vacancies/hidden", async (req, res, next) => {
  try {
    const { offsetId, limit } = req.query;
    logger.trace('api route: /vacancies/hidden with offsetId: %d, limit: %d', offsetId, limit);
    const userId = req.user.id;
    // userId is not optional
    if (!userId) return res.sendStatus(400);

    const results = await database.vacancies.getHidden(
      userId,
      Number(offsetId),
      Number(limit)
    );
    return res.status(200).json(results);
  } catch (err) {
    logger.error('api route: /vacancies/hidden unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.get("/vacancies/pinned", async (req, res, next) => {
  try {
    const { offsetId, limit } = req.query;
    logger.trace('api route: /vacancies/pinned with offsetId: %d, limit: %d', offsetId, limit);
    const userId = req.user.id;
    // userId is not optional
    if (!userId) return res.sendStatus(400);

    const results = await database.vacancies.getPinned(
      userId,
      Number(offsetId),
      Number(limit)
    );
    return res.status(200).json(results);
  } catch (err) {
    logger.error('api route: /vacancies/pinned unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.get("/vacancies", async (req, res, next) => {
  try {
    const { terms, offsetId, limit } = req.query;
    logger.trace('api route: /vacancies with terms: %s, offsetId: %d, limit: %d', terms as string, offsetId, limit);
    const userId = req.user.id;
    // search terms and userId are not optional
    if (!terms || !userId) return res.sendStatus(400);

    database.vacancies.getFts(
      terms as string,
      userId,
      Number(offsetId),
      Number(limit)
    )
      .then((results => {
        logger.trace('api route: /vacancies responding with 200');
        return res.status(200).json(results);
      }))
      .catch((error) => {
        logger.trace('api route: /vacancies responding with 400');
        return res.sendStatus(400);
      })
  } catch (err) {
    logger.error('api route: /vacancies unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});
