import express from "express";
import { db as database, User } from "../database";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { logger } from "../logger";

declare global {
  namespace Express {
    interface Request {
      user: { id: number };
    }
  }
}

let router = express.Router();
export const authentication = router.post("/login", async (req, res, next) => {
  try {
    logger.trace('api route: /login with email: %s', req.body.email);
    // check required fields in body
    const { email, password } = req.body;
    if (!email || !password) return res.sendStatus(400);

    let user: User | null,
      match: boolean,
      token: string,
      payload: { id: number };
    // get user from database
    try {
      user = await database.users.getByEmail(email);
      logger.trace('api route: /login queried user from database: %s', user?.email);
      if (!user) return res.sendStatus(403);
    } catch (err) {
      logger.error('api route: /login failed to query user from database: %s', err.message);
      return res.sendStatus(500);
    }

    // compare pasword hashes
    try {
      match = await bcrypt.compare(password, user.pwhash);
    } catch (err) {
      logger.trace('api route: /login failed to compare password hashes: %s', err.message);
      return res.sendStatus(500);
    }

    // sign token
    payload = { id: user.id };
    if (match) {
      try {
        token = jwt.sign(payload, process.env.JWT_SECRET_PRIVATE!.replace(/\\n/gm, '\n'), {
          algorithm: "RS256",
          expiresIn: process.env.JWT_EXPIRATION,
        });
        logger.trace('api route: /login signed token');
      } catch (err) {
        logger.error('api route: /login failed to sign token: %s', err.message);
        return res.sendStatus(500);
      }
    } else {
      logger.trace('api route: /login failed password check');
      return res.sendStatus(403);
    }

    // if everything checked out, return token cookie
    logger.trace('api route: /login successful login: %s', email);
    return res
      .status(200)
      .cookie("token", token, {
        expires: new Date(
          Date.now() + parseInt(process.env.COOKIE_EXPIRATION!)
        ),
        secure: false,
        httpOnly: true,
      })
      .json(payload);
  } catch (err) {
    logger.error('api route: /login unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.post("/logout", async (req, res, next) => {
  try {
    logger.trace('api route: /logout');
    // unset token cookie
    return res.cookie("token", {}).sendStatus(200);
  } catch (err) {
    logger.error('api route: /logout unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});

router.post("/signup", async (req, res, next) => {
  try {
    logger.trace('api route: /signup with email: %s', req.body.email);
    // check required fields in body
    const { email, password } = req.body;
    if (!email || !password) return res.sendStatus(400);

    // check that email is new
    try {
      let user = await database.users.getByEmail(email);
      logger.trace('api route: /signup failed: email is not unique: %s', req.body.email);
      if (user) return res.sendStatus(400);
    } catch (err) {
      logger.error('api route: /signup failed to query database: %s', err.message);
      return res.sendStatus(500);
    }

    let pwhash;
    // generate hash for the given password
    try {
      pwhash = await bcrypt.hash(
        password,
        parseInt(process.env.JWT_SALT_ROUNDS!, 10)
      );
    } catch (err) {
      logger.error('api route: /signup failed to hash password: %s', err.message);
      return res.sendStatus(500);
    }

    // insert new user into the database
    try {
      await database.users.add(email, pwhash);
    } catch (err) {
      logger.error('api route: /signup failed to insert new user into database: %s', err.message);
      return res.sendStatus(500);
    }

    logger.trace('api route: /signup successfully signed a new user: %s', email);
    // respond to the succesfully handled request
    return res.sendStatus(200);
  } catch (err) {
    logger.error('api route: /signup unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});
