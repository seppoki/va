import express from "express";
import { db as database, User } from "../database";
import { logger } from "../logger";

let router = express.Router();

export const guest = router.get("/guest", async (req, res, next) => {
  try {
    const { terms, offsetId, limit } = req.query;
    logger.trace('api route: /guest with terms: %s, offsetId: %d, limit: %d', terms as string, offsetId, limit);
    // search terms are not optional
    if (!terms) return res.sendStatus(400);

    database.vacancies.getFtsGuest(
      terms as string,
      Number(offsetId),
      Number(limit)
    )
      .then((results => {
        logger.trace('api route: /guest responding with 200');
        return res.status(200).json(results);
      }))
      .catch((error) => {
        logger.trace('api route: /guest responding with 400');
        return res.sendStatus(400);
      })
  } catch (err) {
    logger.error('api route: /guest unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});
