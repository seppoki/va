import express from "express";
import { db as database } from "../database";
import { logger } from "../logger";

let router = express.Router();

export const info = router.get("/info/count", async (req, res, next) => {
  try {
    logger.trace('api route: /info/count');
    const results = await database.info.getCount();
    return res.status(200).json(results);
  } catch (err) {
    logger.error('api route: /info unexpected error: %s', err.message);
    // handle unexpected errors gracefully
    return next(err);
  }
});
