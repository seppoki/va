--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: hides; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hides (
    user_id integer NOT NULL,
    vacancy_id integer NOT NULL
);


--
-- Name: pins; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.pins (
    user_id integer NOT NULL,
    vacancy_id integer NOT NULL
);


--
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    email text NOT NULL,
    pwhash text NOT NULL
);


--
-- Name: user_uid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_uid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_uid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_uid_seq OWNED BY public.users.id;


--
-- Name: vacancies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.vacancies (
    id integer NOT NULL,
    url text NOT NULL,
    header text DEFAULT 'NO VACANCY HEADER'::text,
    ts timestamp with time zone DEFAULT now() NOT NULL,
    contents text NOT NULL,
    source text NOT NULL,
    ts_simple tsvector,
    ts_finnish tsvector,
    ts_english tsvector
);


--
-- Name: vacancy_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.vacancy_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: vacancy_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.vacancy_id_seq OWNED BY public.vacancies.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.user_uid_seq'::regclass);


--
-- Name: vacancies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vacancies ALTER COLUMN id SET DEFAULT nextval('public.vacancy_id_seq'::regclass);


--
-- Name: users unique email; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT "unique email" UNIQUE (email);


--
-- Name: vacancies uniqueurl; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vacancies
    ADD CONSTRAINT uniqueurl UNIQUE (url);


--
-- Name: users user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: vacancies vacancy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.vacancies
    ADD CONSTRAINT vacancy_pkey PRIMARY KEY (id);


--
-- Name: english_gin; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX english_gin ON public.vacancies USING gin (ts_english);


--
-- Name: finnish_gin; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX finnish_gin ON public.vacancies USING gin (ts_finnish);


--
-- Name: hides_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX hides_user_id_index ON public.hides USING btree (user_id);


--
-- Name: hides_vacancy_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX hides_vacancy_id_index ON public.hides USING btree (vacancy_id);


--
-- Name: pins_user_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pins_user_id_index ON public.pins USING btree (user_id);


--
-- Name: pins_vacancy_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX pins_vacancy_id_index ON public.pins USING btree (vacancy_id);


--
-- Name: simple_gin; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX simple_gin ON public.vacancies USING gin (ts_simple);


--
-- Name: vacancies_id_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX vacancies_id_index ON public.vacancies USING btree (id);


--
-- Name: vacancies ts_english_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER ts_english_update BEFORE INSERT OR UPDATE ON public.vacancies FOR EACH ROW EXECUTE FUNCTION tsvector_update_trigger('ts_english', 'pg_catalog.english', 'header', 'contents');


--
-- Name: vacancies ts_finnish_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER ts_finnish_update BEFORE INSERT OR UPDATE ON public.vacancies FOR EACH ROW EXECUTE FUNCTION tsvector_update_trigger('ts_finnish', 'pg_catalog.finnish', 'header', 'contents');


--
-- Name: vacancies ts_simple_update; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER ts_simple_update BEFORE INSERT OR UPDATE ON public.vacancies FOR EACH ROW EXECUTE FUNCTION tsvector_update_trigger('ts_simple', 'pg_catalog.simple', 'header', 'contents');


--
-- Name: hides hide_user-id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hides
    ADD CONSTRAINT "hide_user-id_fkey" FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: hides hide_vacancy-id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hides
    ADD CONSTRAINT "hide_vacancy-id_fkey" FOREIGN KEY (vacancy_id) REFERENCES public.vacancies(id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: pins pins_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pins
    ADD CONSTRAINT pins_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE CASCADE NOT VALID;


--
-- Name: pins pins_vacancy_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pins
    ADD CONSTRAINT pins_vacancy_id_fkey FOREIGN KEY (vacancy_id) REFERENCES public.vacancies(id) ON UPDATE RESTRICT ON DELETE CASCADE NOT VALID;


--
-- PostgreSQL database dump complete
--

